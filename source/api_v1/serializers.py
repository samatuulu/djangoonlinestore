from rest_framework import serializers
from webapp.models import Product


class ProductSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=100, min_length=10)
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'category', 'left', 'price', 'image', 'author')
