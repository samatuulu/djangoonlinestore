from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from api_v1.serializers import ProductSerializer
from webapp.models import Product


class ProductViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
