$("#id_username").change(function () {

    let username = $(this).val();

    $.ajax({
        url: "http://localhost:8000/accounts/ajax/validate_username/",
        data: {
            'username': username
        },
        dataType: 'json',
        success: function (data) {
            if (data.is_taken){
                alert("A user with this username already exists.")
            }

        }
    })

});