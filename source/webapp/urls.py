from django.urls import path
from webapp.views import ProductViewList, ProductDetailView, ProductCreateView, ProductUpdateView, ProductDeleteView, \
    SearchResults, ProductCategoryView, about_us, contact_us

urlpatterns = [
    path('', ProductViewList.as_view(), name='home'),
    path('product/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('product/create/', ProductCreateView.as_view(), name='product_create'),
    path('product/update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('product/delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),
    path('product/search/', SearchResults.as_view(), name='product_search'),
    path('product/', ProductCategoryView.as_view(), name='product_category'),

    path('about_us/', about_us, name='about_us'),
    path('contact_us/', contact_us, name='contact_us'),
]

app_name = 'webapp'
