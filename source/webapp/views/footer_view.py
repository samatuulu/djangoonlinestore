from django.core.mail import send_mail
from django.shortcuts import render

from main.settings import EMAIL_HOST_USER
from webapp import forms


def about_us(request):
    return render(request, 'about_us/about_us.html')


def contact_us(request):
    sub = forms.Subscribe()
    if request.method == 'POST':
        sub = forms.Subscribe(request.POST)
        subject = 'Thank you for getting in touch!'
        message = 'We appreciate you contacting us Tanze. One of our colleagues will get back in touch with you soon!'
        recepient = str(sub['email'].value())
        send_mail(subject,
                  message, EMAIL_HOST_USER, [recepient], fail_silently=False)
        return render(request, 'contact_us/contact_us.html', {'recepient': recepient})
    return render(request, 'contact_us/index.html', {'form': sub})
