from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from webapp.forms import ProductForm
from webapp.models import Product


class ProductViewList(ListView):
    model = Product
    template_name = 'product/product_index.html'
    context_object_name = 'products'
    paginate_by = 6
    paginate_orphans = 1

    def get_queryset(self):
        product = Product.objects.exclude(left=0)
        return product


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/product_detail.html'
    context_object_name = 'product'


class ProductCreateView(PermissionRequiredMixin, CreateView):
    model = Product
    template_name = 'product/product_create.html'
    form_class = ProductForm
    permission_required = 'webapp.add_product'
    permission_denied_message = 'Accces denided.'

    def form_valid(self, form):
        if str(self.request.user) != 'AnonymousUser':
            form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:product_detail', kwargs={'pk': self.object.pk})


class ProductUpdateView(UserPassesTestMixin, UpdateView):
    model = Product
    template_name = 'product/product_update.html'
    fields = ('name', 'description', 'category', 'left', 'price', 'image')
    context_object_name = 'product'

    def test_func(self):
        if self.request.user.has_perm('change_product') or self.get_object().author == self.request.user:
            return True

    def get_success_url(self):
        return reverse('webapp:product_detail', kwargs={'pk': self.object.pk})


class ProductDeleteView(UserPassesTestMixin, DeleteView):
    model = Product
    template_name = 'product/product_delete.html'
    context_object_name = 'product'
    success_url = reverse_lazy('webapp:home')

    def test_func(self):
        if self.request.user.has_perm('delete_product') or self.get_object().author == self.request.user:
            return True

    def delete(self, request, *args, **kwargs):
        product = self.object = self.get_object()
        product.delete()
        return HttpResponseRedirect(self.get_success_url())


class ProductCategoryView(ListView):
    model = Product
    template_name = 'product/product_category.html'
    context_object_name = 'products'
    paginate_by = 6
    paginate_orphans = 1
    page_kwarg = 'category'

    def get_queryset(self):
        query = self.request.GET.get('list')
        product = Product.objects.all().filter(category__contains=query)
        return product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = self.request.GET.get('list')
        return context


class SearchResults(ListView):
    model = Product
    template_name = 'product/search_results.html'
    ordering = ('name',)
    paginate_by = 6
    paginate_orphans = 1
    page_kwarg = 'product'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query == "":
            messages.add_message(self.request, messages.WARNING, 'Please, fill out the search bar.')
        object_list = Product.objects.filter(
            Q(name__icontains=query)
        )
        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        products = Product.objects.all()
        paginator = Paginator(products, 6)
        page = self.request.GET.get('page')
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        context['products'] = products
        context['query'] = self.request.GET.get('q')
        return context
