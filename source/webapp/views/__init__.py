from .product_view import ProductViewList, ProductDetailView, ProductCreateView, ProductUpdateView, ProductDeleteView, \
    SearchResults, ProductCategoryView
from .footer_view import about_us, contact_us
