from django.db import models

CATEGORY_CHOICES = (
    ('other_category', 'Other'),
    ('cloth_category', 'Clothes'),
    ('food_category', 'Food'),
    ('household_category', 'Household items'),
    ('electronic', 'Computers'),
)


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name='Product name:')
    description = models.CharField(max_length=2000, null=True, blank=True, verbose_name='Product description:')
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES,
                                default=CATEGORY_CHOICES[0][0], verbose_name='Category:')
    left = models.PositiveIntegerField(verbose_name='How much left..')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Product price:')
    image = models.ImageField(upload_to='product_images', null=False, blank=False, verbose_name='Product image:')
    author = models.ForeignKey('auth.User', related_name='product',
                               on_delete=models.CASCADE, null=True, blank=True, verbose_name='Author')

    class Meta:
        ordering = ('category', 'name',)

    def __str__(self):
        return self.name
