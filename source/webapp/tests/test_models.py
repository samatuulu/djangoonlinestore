from django.test import TestCase
from django.urls import reverse

from webapp.models import Product


class ProductModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Product.objects.create(name='iPhone', description='iPhone is a nice phone.',
                               category='electronic',
                               left=1, price=23, image='product_images/apple-mouse.jpg')

    def setUp(self):
        self.product = Product.objects.get(id=1)

    def tearDown(self):
        self.product.delete()

    def test_name_label(self):
        field_label = self.product._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'Product name:')

    def test_description_label(self):
        field_label = self.product._meta.get_field('description').verbose_name
        self.assertEquals(field_label, 'Product description:')

    def test_category_label(self):
        field_label = self.product._meta.get_field('category').verbose_name
        self.assertEquals(field_label, 'Category:')

    def test_product_list_view(self):
        response = self.client.get(reverse('webapp:home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product_index.html')
