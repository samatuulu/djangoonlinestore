from django import forms
from django.core.exceptions import ValidationError

from webapp.models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'category', 'left', 'price', 'image']

    def clean_name(self):
        name = self.cleaned_data['name']
        if len(name) <= 10:
            raise ValidationError("This field should be more than 10 symbols.", code='too_short')
        return name


class Subscribe(forms.Form):
    email = forms.EmailField()

    def __str__(self):
        return self.email
