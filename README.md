## Django Online Store

This project is all about online store. Project is not completely covers all the logics of online store but I am pretty sure this project is covers basic logic of online store. 
For example, you can do all the CRUD (which is stands for CREATE READ UPDATE DELETE) in Django and other interesing part also covers, like sending an email to users.

### Built With
This project built with Django webframework. Also, contains like Bootstrap, HTML&CSS interesting markup language and stylesheet for own design and managing.
* [Bootstrap](https://getbootstrap.com)
* [Django3.0](https://docs.djangoproject.com/en/3.0/)
* [HTML&CSS](https://www.w3schools.com/html/html_css.asp)

## Getting Started

Here is line by line steps how you can run this project in your local machine. I assume you already have Python3. Make sure to follow each steps and you are good to go.

### Prerequisites

First thing is first. In order to install local environment. Follow up some commands below. If those commands not works for installing local environment, try to google, how to install environment properly. I'll also leave link about local environment below.

Remember, you can give a name for your local environment.

* venv (feel free to give any name you like) but I named as venv.
```shell script
python3 -m virtualenv -p python3 venv
```
<h6>NOTE: This command works for me but if you are getting error while installation process of local environment check the link below.</h6>

After when you installed local environment you need to activate it.
```shell script
. venv/bin/activate
```
<h6>NOTE: Make sure your python in right path and version by typing `which python` and `python -V`. You should see output after this command.</h6>

### Installation

1.Clone the repo
```shell script
git clone https://gitlab.com/samatuulu/djangoonlinestore.git
```
<h6>IMPORTANT: Your local environment needs to be active for all of these steps below.</h6>

2.Go go the project folder by typing this command ```cd djangoonlinestore``` and there you should see other project folders/files by typing `ls` command.

3.Install packages
```shell script
pip install -r requirements.txt
```
4.Now go to that file where is `manage.py` python file is located. For example: ```cd source``` and inside `source` folder you can see the file.<br/>

5.Make sure you did migrations and migrate.
```shell script
python3 manage.py makemigrations
python3 manage.py migrate
```

6.Next, you need to unzip file ```images_static``` zip file to the directory to ``source/webapp/static``. This step needs for showing static images of website. Zip file locates in `source/webapp/fixtures/`

7.1. This step is optional. If you want to back up my database you can run this command
```shell script
python3 manage.py loaddata webapp/fixtures/dump.json
``` 
7.2 Next you need to unzip folder `product_images.zip` to directory `source/uploads`. Folder `product_images.zip` locates inside the folder `source/webapp/fixtures`. And remember to create `uploads` folder in directory `source`<br/>

7.3 For superuser: login `admin` and password `admin`.

8.Finally, you need to run server by typing this command.
```shell script
python3 manage.py runserver
```

After running run server command check your browser on host: http://localhost:8000 and you should see the main page of the project.

### Docker

Do you know? You can also run project in Docker.

NOTE: In order to run project under Docker, you need have Docker and Docker Compose in your OS. I''ll also leave link below in details.

To do this. Just type simple command below. 

```shell script
docker-compose up --build
```

This command runs project under Docker and connects on port 8000. Now, you can check http://localhost:8000.


## Resources
A nice doc about environment.
* [Python environment (locally)](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment)

Read about how run Django project in Docker container.
* [Docker with Django](https://docs.docker.com/compose/django/)

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/amazingfeature`)
3. Commit your Changes (`git commit -m 'add some amazingfeature'`)
4. Push to the Branch (`git push origin feature/amazingfeature`)
5. Open a Pull Request

<!-- CONTACT -->
## Contact

Bektursun - [GitLab](https://gitlab.com/samatuulu) - samatuulu11@gmail.com
